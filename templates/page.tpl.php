<?php $node = menu_get_object(); ?>

<div role="document" class="page">

  <header role="banner" class="l-header">

    <!-- TOP BAR - FOR MOBILE -->
    <div class="<?php print $top_bar_classes; ?>">
	    <nav class="top-bar" data-topbar <?php print $top_bar_options; ?>>
		    <!-- BRANDING -->
	      <ul class="title-area">
	        <li class="name">
	        	<h1><a href="https://www.unt.edu" title="UNT" aria-label="UNT home page link" target="_blank">UNT</a></h1>
	        </li>
	        <li class="toggle-topbar menu-icon">
	          <a href="#" aria-label="Mobile Menu toggle button"><span class="text-indent">Menu</span><span><?php print $top_bar_menu_text; ?></span></a>
	        </li>
	      </ul>
	      <!-- /BRANDING -->

				<!-- MOBILE MENU -->
        <!-- <section class="top-bar-section"> -->
        <div class="top-bar-section">
	        <?php if ($top_bar_main_menu) : ?>
	          <?php print $top_bar_main_menu; ?>
	        <?php endif; ?>
	      </div>
	      <!-- /MOBILE MENU  -->
	    </nav>
    </div>
    <!-- /TOP BAR -->

		<!-- HEADER -->
    <!-- <section id="header" class="row <?php //print $alt_header_classes; ?>"> -->
    <div id="header" class="row <?php print $alt_header_classes; ?>">

			<div id="banner">
				<a href="http://www.unt.edu/" title="UNT Home Site" target="_blank" aria-label="UNT home page link"><img src="//webassets.unt.edu/images/d7foundation/unt-1890-banner.svg" alt="UNT | Est. 1890" class="unt-banner"></a>
			</div>

			<div id="lettermark">
				<a href="http://www.unt.edu" aria-label="UNT home page link"><img src="//webassets.unt.edu/images/d7foundation/unt_lettermark.svg" alt="UNT"></a>
			</div>

			<div id="site-name">
				<?php if(variable_get('wdc_department_name') && variable_get('wdc_department_url')): ?>
					<span>
            <a href="<?php echo variable_get('wdc_department_url'); ?>" aria-label="Link to <?php echo variable_get('wdc_department_name'); ?>"><?php echo variable_get('wdc_department_name'); ?></a>
          </span>
				<?php elseif(variable_get('wdc_department_name')): ?>
					<span><?php echo variable_get('wdc_department_name'); ?></span>
				<?php endif; ?>
				<h1 class="title"><?php print $linked_site_name; ?></h1>
			</div>

    	<div id="search">
				<a id="searchbutton" data-dropdown="searchdrop" data-options="is_hover:true; hover_timeout:250" aria-controls="searchdrop" aria-expanded="false" aria-label="Search form toggle"><img src="//webassets.unt.edu/images/d7foundation/fa-search.svg" alt="Search button" /></a>

				<div id="searchdrop" data-dropdown-content class="small f-dropdown" aria-hidden="true" tabindex="-1">
				  <?php //print $search; ?>
          <?php $form = drupal_get_form('search_block_form', TRUE); ?>
          <?php print render($form); ?>
				</div>
    	</div>

    	<div id="quicklinks">
				<a id="quickbutton" data-dropdown="quickdrop" data-options="is_hover:true; hover_timeout:250" aria-controls="quickdrop" aria-expanded="false" aria-label="Quicklinks menu toggle"><img src="//webassets.unt.edu/images/d7foundation/fa-quicklinks.svg" alt="Quicklinks button" /></a>

				<ul id="quickdrop" class="small f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
				  <li><a href="//my.unt.edu/" class="alt">MyUNT</a></li>
				  <li><a href="//learn.unt.edu/" class="alt">Blackboard</a></li>
				  <li><a href="//eagleconnect.unt.edu/" class="alt">EagleConnect</a></li>
				  <li><a href="//www.unt.edu/find-people-departments" class="alt">UNT Directory</a></li>
				  <li><a href="//maps.unt.edu/" class="alt">UNT Map</a></li>
				  <li><a href="//hr.untsystem.edu/" class="alt">Jobs at UNT</a></li>
				</ul>
    	</div>

    	<div id="search-scrolled">
				<a id="searchscrolledbutton" data-dropdown="searchdrop-scrolled" data-options="is_hover:true; hover_timeout:250" aria-controls="searchdrop-scrolled" aria-expanded="false" aria-label="Scrolled screen Search form toggle"><img src="//webassets.unt.edu/images/d7foundation/fa-search.svg" alt="Search button" /></a>
    	</div>

    	<div id="quicklinks-scrolled">
				<a id="quickscrolledbutton" data-dropdown="quickdrop-scrolled" data-options="is_hover:true; hover_timeout:250" aria-controls="quickdrop-scrolled" aria-expanded="false" aria-label="Scrolled screen Quicklinks menu toggle"><img src="//webassets.unt.edu/images/d7foundation/fa-quicklinks.svg" alt="Quicklinks button" /></a>
    	</div>

    	<div id="menu-scrolled">
				<a id="menuscrolledbutton" data-dropdown="menudrop-scrolled" data-options="is_hover:true; hover_timeout:250" aria-controls="menudrop-scrolled" aria-expanded="false" aria-label="Scrolled screen Main Menu toggle"><img src="//webassets.unt.edu/images/d7foundation/fa-menu.svg" alt="Main Menu button" /></a>
    	</div>

      <?php print render($page['branding']); ?>

    </div>
    <!-- /HEADER -->

    <?php if (!empty($page['header'])): ?>
      <!-- <section class="l-header-region row"> -->
      <div class="l-header-region row">
        <div class="columns">
          <?php print render($page['header']); ?>
        </div>
      </div>
    <?php endif; ?>

  	<div class="main-menu">
    	<div class="box">
	    	<div class="large-12">
	    		<?php print render($page['main_menu']); ?>
	    	</div>
    	</div>
  	</div>

  </header>
  <!-- END OF "IF TOP BAR" -->

	<div id="dropbar">
		<div id="quickdrop-scrolled" class="dropbar-menu-container box mega f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
			<div class="row">
				<ul class="links">
					<li><a href="//my.unt.edu/" class="alt">MyUNT</a></li>
					<li><a href="//learn.unt.edu/" class="alt">Blackboard</a></li>
					<li><a href="//eagleconnect.unt.edu/" class="alt">EagleConnect</a></li>
					<li><a href="//www.unt.edu/find-people-departments" class="alt">UNT Directory</a></li>
					<li><a href="//maps.unt.edu/" class="alt">UNT Map</a></li>

					<li><a href="//hr.untsystem.edu/" class="alt">Jobs at UNT</a></li>
				</ul>
			</div>
		</div>

		<div id="searchdrop-scrolled" class="mega f-dropdown content" data-dropdown-content aria-hidden="true" tabindex="-1">
			<div class="box">
				<?php //print $search; ?>
        <?php $form = drupal_get_form('search_block_form', TRUE); ?>
        <?php print render($form); ?>
			</div>
		</div>

		<div id="menudrop-scrolled" class="dropbar-menu-container box mega f-dropdown" data-dropdown-content="" aria-hidden="true" tabindex="-1">
			<div class="row">
				<?php //print $scrolled_menu; ?>
	      <?php
					$menu = menu_navigation_links('main-menu');
					print theme('links__system_main_menu', array('links' => $menu));
				?>
			</div>
		</div>
	</div>
	<div id="mobile-site-name">
		<?php if($site_name): ?>
			<a href="/"><h1 class="mobile-site-name"><?php echo $site_name; ?></h1></a>
		<?php endif; ?>
	</div>

	<?php if(!empty($page['hero'])): ?>
		<div id="hero-feature">
			<?php print render($page['hero']); ?>
		</div>
	<?php endif; ?>

	<?php if(!empty($page['hero_grid'])): ?>
		<div id="hero-feature contained">
			<div class="box">
				<div class="row">
					<?php print render($page['hero_grid']); ?>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<?php if(isset($node->field_wide_layout) && isset($node->field_wide_layout[$node->language]) && $node->field_wide_layout[$node->language][0]['value']==1): ?>
		<div class="wide-body">
		<?php if ($breadcrumb): print '<div id="breadcrumbs" class="box">' . $breadcrumb . '</div>'; endif; ?>
	<?php else: ?>
		<?php if ($breadcrumb): print '<div id="breadcrumbs" class="box">' . $breadcrumb . '</div>'; endif; ?>
	<?php endif; ?>

  <?php if (!empty($page['featured'])): ?>
    <!--.l-featured -->
    <!-- <section class="l-featured row"> -->
    <div class="l-featured row">
      <div class="columns">
        <?php print render($page['featured']); ?>
      </div>
    </div>
    <!--/.l-featured -->
  <?php endif; ?>

  <?php if ($messages && !$zurb_foundation_messages_modal): ?>
    <!--.l-messages -->
    <!-- <section class="l-messages row"> -->
    <div class="l-messages row">
      <div class="columns">
        <?php if ($messages): print $messages; endif; ?>
      </div>
    </div>
    <!--/.l-messages -->
  <?php endif; ?>

  <?php if (!empty($page['help'])): ?>
    <!--.l-help -->
    <!-- <section class="l-help row"> -->
    <div class="l-help row">
      <div class="columns">
        <?php print render($page['help']); ?>
      </div>
    </div>
    <!--/.l-help -->
  <?php endif; ?>

  <!--.l-main -->
  <main role="main" class="row l-main">
    <!-- .l-main region -->
    <div class="<?php print $main_grid; ?> main columns">
      <?php if (!empty($page['highlighted'])): ?>
        <div class="highlight panel callout">
          <?php print render($page['highlighted']); ?>
        </div>
      <?php endif; ?>

      <a id="main-content"></a>


      <?php if ($title): ?>
				<?php if(empty($node->field_hide_page_title) || (isset($node->field_hide_page_title[$node->language]) && $node->field_hide_page_title[$node->language][0]['value']==0)): ?>
					<?php if(!empty($node->field_adjust_page_title) && $node->field_adjust_page_title[$node->language][0]['value']==1): ?>
						<div class="row">
							<h1 id="page-title" class="title adjust"><?php print $title; ?></h1>
						</div>
					<?php else: ?>
						<h1 id="page-title" class="title"><?php print $title; ?></h1>
					<?php endif; ?>
				<?php endif; ?>
      <?php endif; ?>

      <?php //gaa if (!empty($tabs)): ?>
			<?php //drupal_set_message(print_r($tabs, true)); ?>
      <?php //gaa if (user_is_logged_in() && !empty($tabs)): ?>
      <?php if ($tabs && !empty($tabs['#primary'])): ?>
      	<div class="tabs">
	        <?php print render($tabs); ?>
	        <?php if (!empty($tabs2)): print render($tabs2); endif; ?>
      	</div>
      <?php endif; ?>

      <?php if ($action_links): ?>
        <ul class="action-links">
          <?php print render($action_links); ?>
        </ul>
      <?php endif; ?>

      <?php print render($page['content']); ?>

      <div class="jump-to-top"><a href="#skip"><span>Top</span></a></div>
    </div>
    <!--/.l-main region -->

    <?php if (!empty($page['sidebar_first'])): ?>
      <aside role="complementary" class="<?php print $sidebar_first_grid; ?> sidebar-first columns sidebar">
        <?php print render($page['sidebar_first']); ?>
      </aside>
    <?php endif; ?>

    <?php if (!empty($page['sidebar_second'])): ?>
      <aside role="complementary" class="<?php print $sidebar_sec_grid; ?> sidebar-second columns sidebar">
        <?php print render($page['sidebar_second']); ?>
      </aside>
    <?php endif; ?>
  </main>
  <!--/.l-main -->

  <?php if (!empty($page['triptych_first']) || !empty($page['triptych_middle']) || !empty($page['triptych_last'])): ?>
    <!--.triptych-->
    <!-- <section class="l-triptych row"> -->
    <div class="l-triptych row">
      <div class="triptych-first medium-4 columns">
        <?php print render($page['triptych_first']); ?>
      </div>
      <div class="triptych-middle medium-4 columns">
        <?php print render($page['triptych_middle']); ?>
      </div>
      <div class="triptych-last medium-4 columns">
        <?php print render($page['triptych_last']); ?>
      </div>
    </div>
    <!--/.triptych -->
  <?php endif; ?>

  <?php
  	if(isset($node->field_wide_layout) && isset($node->field_wide_layout[$node->language]) && $node->field_wide_layout[$node->language][0]['value']==1):
  ?>
	</div><!-- CLOSES WIDE BODY -->
<?php endif; ?>

<footer id="footer">
  <!-- <section class="footer-socials"> -->
  <div class="footer-socials">
		<div class="socials-wrapper">
			<?php if (variable_get('wdc_facebook')): ?>
				<a href="<?php echo variable_get('wdc_facebook'); ?>" class="facebook" title="Facebook" target="_blank" aria-label="<?php print $site_name; ?> on Facebook"><img src="//webassets.unt.edu/images/d7foundation/social-facebook.png" alt="facebook"></a>
			<?php else: ?>
				<a href="https://www.facebook.com/northtexas" class="facebook" title"UNT on facebook" target="_blank" aria-label="UNT on Facebook"><img src="//webassets.unt.edu/images/d7foundation/social-facebook.png" alt="facebook"></a>
			<?php endif; ?>

			<?php if (variable_get('wdc_twitter')): ?>
				<a href="<?php echo variable_get('wdc_twitter'); ?>" class="twitter" title="Twitter" target="_blank" aria-label="<?php print $site_name; ?> on Twitter"><img src="//webassets.unt.edu/images/d7foundation/social-twitter.png" alt="twitter"></a>
			<?php else: ?>
				<a href="https://twitter.com/UNTSocial" class="twitter" title="Twitter" target="_blank" aria-label="UNT on Twitter"><img src="//webassets.unt.edu/images/d7foundation/social-twitter.png" alt="twitter"></a>
			<?php endif; ?>

			<?php if (variable_get('wdc_youtube')): ?>
				<a href="<?php echo variable_get('wdc_youtube'); ?>" class="youtube" title="YouTube" target="_blank" aria-label="<?php print $site_name; ?> on YouTube"><img src="//webassets.unt.edu/images/d7foundation/social-youtube.png" alt="youtube"></a>
			<?php else: ?>
				<a href="https://www.youtube.com/user/universitynorthtexas" class="youtube" title="YouTube" target="_blank" aria-label="UNT on YouTube"><img src="//webassets.unt.edu/images/d7foundation/social-youtube.png" alt="YouTube"></a>
			<?php endif; ?>

			<?php if (variable_get('wdc_vimeo')): ?>
				<a href="<?php echo variable_get('wdc_vimeo'); ?>" class="vimeo" title="Vimeo" target="_blank" aria-label="<?php print $site_name; ?> on Vimeo"><img src="//webassets.unt.edu/images/d7foundation/social-vimeo.png" alt="Vimeo"></a>
			<?php endif; ?>

			<?php if (variable_get('wdc_instagram')): ?>
				<a href="<?php echo variable_get('wdc_instagram'); ?>" class="instagram" title="Instagram" target="_blank" aria-label="<?php print $site_name; ?> on Instagram"><img src="//webassets.unt.edu/images/d7foundation/social-instagram.png" alt="Instagram"></a>
			<?php else: ?>
				<a href="https://www.instagram.com/UNT/" class="instagram" title="Instagram" target="_blank" aria-label="UNT on Instagram"><img src="//webassets.unt.edu/images/d7foundation/social-instagram.png" alt="Instagram"></a>
			<?php endif; ?>

			<?php if (variable_get('wdc_flickr')): ?>
				<a href="<?php echo variable_get('wdc_flickr'); ?>" class="flickr" title="Flickr" target="_blank" aria-label="<?php print $site_name; ?> on Flickr"><img src="//webassets.unt.edu/images/d7foundation/social-flickr.png" alt="flickr"></a>
			<?php else: ?>
				<a href="https://www.flickr.com/photos/unt" class="flickr" title="flickr" target="_blank" aria-label="UNT on Flickr"><img src="//webassets.unt.edu/images/d7foundation/social-flickr.png" alt="flickr"></a>
			<?php endif; ?>

			<?php if (variable_get('wdc_pinterest')): ?>
				<a href="<?php echo variable_get('wdc_pinterest'); ?>" class="pinterest" title="Pinterest" target="_blank" aria-label="<?php print $site_name; ?> on Pinterest"><img src="//webassets.unt.edu/images/d7foundation/social-pinterest.png" alt="Pinterest"></a>
			<?php endif; ?>

			<?php if (variable_get('wdc_linkedin')): ?>
				<a href="<?php echo variable_get('wdc_linkedin'); ?>" class="linkedin" title="LinkedIn" target="_blank" aria-label="<?php print $site_name; ?> on LinkedIn"><img src="//webassets.unt.edu/images/d7foundation/social-linkedin.png" alt="LinkedIn"></a>
			<?php endif; ?>

			<a href="http://social.unt.edu/social-media-directory" class="directory" title="UNT Social Media Directory" aria-label="UNT Social Media directory"><img src="//webassets.unt.edu/images/d7foundation/social-directory.png" alt="UNT Social Directory"></a>
		</div>
	</div>

  <!-- <section class="footer-main"> -->
  <div class="footer-main">
    <div class="wrapper row l-footer-columns">

      <div class="footer-first medium-3 columns">
        <div class="footer-brand">
					<a href="https://www.unt.edu" aria-label="UNT home page link">
						<img src="//webassets.unt.edu/images/d7foundation/unt-logo-stacked-green.svg" alt="UNT | University of North Texas | green logo" class="desktop" title="UNT | University of North Texas home page" />
						<img src="//webassets.unt.edu/images/d7foundation/unt-logo-stacked.svg" alt="UNT | University of North Texas | white logo" class="mobile" title="UNT | University of North Texas home page" />
					</a>
				</div>
      </div>

      <div class="footer-second medium-6 mobile-0 desktop columns">
        <!-- <h4 class="title">Main Menu</h4>
        <?php
					//$menu = menu_navigation_links('main-menu');
					//print theme('links__system_main_menu', array('links' => $menu));
				?> -->

        <h4 class="title">University Links</h4>
        <ul class="links">
				  <li><a href="//my.unt.edu/" class="alt">MyUNT</a></li>
				  <li><a href="//learn.unt.edu/" class="alt">Blackboard</a></li>
				  <li><a href="//eagleconnect.unt.edu/" class="alt">EagleConnect</a></li>
				  <li><a href="//www.unt.edu/find-people-departments" class="alt">UNT Directory</a></li>
				  <li><a href="//maps.unt.edu/" class="alt">UNT Map</a></li>
				  <li><a href="//hr.untsystem.edu/" class="alt">Jobs at UNT</a></li>
        </ul>

				<h4 class="title">Email &amp; Phone</h4>

				<ul class="links">
					<?php if (variable_get('wdc_contact_email') || variable_get('wdc_email_2') || variable_get('wdc_email_3')): ?>
						<?php if(variable_get('wdc_contact_email')): ?>
							<li><i class="fa fa-envelope"></i>&nbsp;<a href="mailto:<?php echo variable_get('wdc_contact_email'); ?>" class="email"><?php echo variable_get('wdc_contact_email'); ?></a></li>
						<?php endif; ?>
						<?php if(variable_get('wdc_email_2')): ?>
							<li><i class="fa fa-envelope"></i>&nbsp;<a href="mailto:<?php echo variable_get('wdc_email_2'); ?>" class="email"><?php echo variable_get('wdc_email_2'); ?></a></li>
						<?php endif; ?>
						<?php if(variable_get('wdc_email_3')): ?>
							<li><i class="fa fa-envelope"></i>&nbsp;<a href="mailto:<?php echo variable_get('wdc_email_3'); ?>" class="email"><?php echo variable_get('wdc_email_3'); ?></a></li>
						<?php endif; ?>
					<?php else: ?>
						<li><i class="fa fa-envelope"></i>&nbsp;<a href="mailto:urcm@unt.edu">urcm@unt.edu</a></li>
					<?php endif; ?>

					<?php if(variable_get('wdc_contact_phone') || variable_get('wdc_phone_2') || variable_get('wdc_phone_3')): ?>
						<?php if(variable_get('wdc_contact_phone')): ?>
							<li><i class="fa fa-phone"></i>&nbsp;<a href="tel:<?php echo variable_get('wdc_contact_phone'); ?>"><?php echo variable_get('wdc_contact_phone'); ?></a></li>
						<?php endif; ?>
						<?php if(variable_get('wdc_phone_2')): ?>
							<li><i class="fa fa-phone"></i>&nbsp;<a href="tel:<?php echo variable_get('wdc_phone_2'); ?>"><?php echo variable_get('wdc_phone_2'); ?></a></li>
						<?php endif; ?>
						<?php if(variable_get('wdc_phone_3')): ?>
							<li><i class="fa fa-phone"></i>&nbsp;<a href="tel:<?php echo variable_get('wdc_phone_3'); ?>"><?php echo variable_get('wdc_phone_3'); ?></a></li>
						<?php endif; ?>
					<?php else: ?>
						<li><i class="fa fa-phone"></i>&nbsp;<a href="tel:940.565.2000">940-565-2000</a></li>
						<li><i class="fa fa-tty"></i>&nbsp;<a href="tel:940.369.8652">940-369-8652</a></li>
					<?php endif; ?>
				</ul>
      </div>

      <div class="footer-fifth medium-3 footer-contact columns">

        <p class="footer-site-name"><?php echo $site_name; ?></p>

				<p class="address">
					<i class="fa fa-map-marker"></i>&nbsp;&nbsp;<a href="<?php if (variable_get('wdc_google_link')): echo variable_get('wdc_google_link'); else: echo '//goo.gl/maps/q86wcXbNukN2'; endif; ?>" target="_blank"

            aria-label="<?php if (variable_get('wdc_google_link')) {
              if (variable_get('wdc_location')) {
                echo variable_get('wdc_location');
              } else {
                echo $site_name;
              };
            } else {
              echo 'UNT';
            } ?> on Google Maps">

            <?php if (variable_get('wdc_google_link')) {
              if (variable_get('wdc_location')) {
                echo variable_get('wdc_location');
              } else {
                echo $site_name;
              };
            } else {
              echo 'UNT';
            } ?></a><br />

					<?php if(variable_get('wdc_address_1')): echo '<i class="fa fa-envelope"></i>&nbsp;&nbsp;' . variable_get('wdc_address_1'); else: echo '<i class="fa fa-envelope"></i>&nbsp;&nbsp;1155 Union Circle #311277'; endif; ?><br />
					<?php if(variable_get('wdc_address_2')): echo variable_get('wdc_address_2') . '<br />'; endif; ?>
					<?php if(variable_get('wdc_address_city')): echo variable_get('wdc_address_city'); else: echo 'Denton'; endif; ?>, <?php if(variable_get('wdc_address_state')): echo variable_get('wdc_address_state'); else: echo 'Texas'; endif; ?> <?php if(variable_get('wdc_address_zip')): echo variable_get('wdc_address_zip'); else: echo '76203-5017'; endif; ?><br />
					<i class="fa fa-car"></i>&nbsp;&nbsp;<a href="https://www.unt.edu/community">Visitor Information</a></p>

				<div class="mobile-ctas">
					<?php if(variable_get('wdc_contact_phone')): ?>
						<a class="call-us" href="tel:<?php echo variable_get('wdc_contact_phone'); ?>"><i class="fa fa-phone" title="Call us"></i><br />Call us</a>
						<?php else: ?>
						<a class="call-us" href="tel:9405662000"><i class="fa fa-phone" title="Call us"></i><br />Call us</a>
					<?php endif; ?>

					<?php if(variable_get('wdc_contact_email')): ?>
						<a class="email-us" href="mailto:<?php echo variable_get('wdc_contact_email'); ?>"><i class="fa fa-envelope" title="Email us"></i><br />Email Us</a>
						<?php else: ?>
						<a class="email-us" href="mailto:urcm@unt.edu"><i class="fa fa-envelope" title="Email us"></i><br />Email Us</a>
					<?php endif; ?>

					<a class="unt-map" href="https://goo.gl/maps/gmkVRXCJqCq" title="UNT Map"><i class="fa fa-map-marker"></i><br />UNT Map</a>
				</div>
      </div>
    </div>
  </div>

  <!-- <section id="mobile-socials"> -->
  <div id="mobile-socials">
		<?php if (variable_get('wdc_facebook')): ?>
			<a href="<?php echo variable_get('wdc_facebook'); ?>" id="tab1" class="tab facebook" title="facebook"><i class="fa fa-facebook"></i><span class="label-hide">facebook</span></a>
		<?php else: ?>
			<a href="https://www.facebook.com/northtexas" id="tab1" class="tab facebook" title="facebook"><i class="fa fa-facebook"></i><span class="label-hide">facebook</span></a>
		<?php endif; ?>

		<?php if (variable_get('wdc_twitter')): ?>
			<a href="<?php echo variable_get('wdc_twitter'); ?>" id="tab2" class="tab twitter" title="twitter"><i class="fa fa-twitter"></i><span class="label-hide">twitter</span></a>
		<?php else: ?>
			<a href="https://twitter.com/UNTSocial" id="tab2" class="tab twitter" title="twitter"><i class="fa fa-twitter"></i><span class="label-hide">twitter</span></a>
		<?php endif; ?>

		<?php if (variable_get('wdc_instagram')): ?>
			<a href="<?php echo variable_get('wdc_instagram'); ?>" id="tab3" class="tab instagram" title="Instagram"><i class="fa fa-instagram"></i><span class="label-hide">Instagram</span></a>
		<?php else: ?>
			<a href="https://www.instagram.com/UNT/" id="tab3" class="tab instagram" title="Instagram"><i class="fa fa-instagram"></i><span class="label-hide">Instagram</span></a>
		<?php endif; ?>

		<?php if (variable_get('wdc_flickr')): ?>
			<a href="<?php echo variable_get('wdc_flickr'); ?>" id="tab4" class="tab flickr" title="flickr"><i class="fa fa-flickr"></i><span class="label-hide">Flickr</span></a>
		<?php endif; ?>

		<?php if (variable_get('wdc_pinterest')): ?>
			<a href="<?php echo variable_get('wdc_pinterest'); ?>" id="tab5" class="tab pinterest" title="Pinterest"><i class="fa fa-pinterest"></i><span class="label-hide">Pinterest</span></a>
		<?php endif; ?>

		<?php if (variable_get('wdc_youtube')): ?>
			<a href="<?php echo variable_get('wdc_youtube'); ?>" id="tab6" class="tab youtube" title="YouTube"><i class="fa fa-youtube"></i><span class="label-hide">YouTube</span></a>
		<?php else: ?>
			<a href="https://www.youtube.com/user/universitynorthtexas" id="tab6" class="tab youtube" title="YouTube"><i class="fa fa-youtube"></i><span class="label-hide">YouTube</span></a>
		<?php endif; ?>

		<?php if (variable_get('wdc_vimeo')): ?>
			<a href="<?php echo variable_get('wdc_vimeo'); ?>" id="tab7" class="tab vimeo" title="Vimeo"><i class="fa fa-vimeo"></i><span class="label-hide">Vimeo</span></a>
		<?php endif; ?>

		<?php if (variable_get('wdc_linkedin')): ?>
			<a href="<?php echo variable_get('wdc_linkedin'); ?>" id="tab8" class="tab linkedin" title="LinkedIn"><i class="fa fa-linkedin"></i><span class="label-hide">LinkedIn</span></a>
		<?php endif; ?>

			<a href="http://social.unt.edu/social-media-directory" id="tab9" class="tab directory" title="UNT Social Media Directory"><i class="fa fa-list-ul"></i><span class="label-hide">UNT Media Direction</span></a>
  </div>

  <!-- <section class="copyright"> -->
  <div class="copyright">
		<div class="col large-12 centered">
			<div class="btn btn1 c1 altcolor small">
				<a href="//admissions.unt.edu">Apply now</a>
				<a href="//tours.unt.edu">Schedule a tour</a>
				<a href="//admissions.unt.edu/requestinfo">Get more info</a>
			</div>
		</div>

		<p class="footer-copylinks"><a href="//www.unt.edu/disclaimer">Disclaimer</a> | <a href="//www.unt.edu/ada">AA/EOE/ADA</a> | <a href="//www.unt.edu/privacy">Privacy</a> | <a href="//policy.unt.edu/policy/14-005">Electronic Accessibility</a> | <a href="//www.unt.edu/required-links">Required Links</a> | <a href="//www.unt.edu">UNT Home</a></p>
		<p>&copy;<?php print date("Y") ?> University of North Texas</p>
		<p class="show-for-small-only"><a href="//www.unt.edu/required-links">Required Links</a><span></p>

	</div>

</footer>

<?php if ($messages && $zurb_foundation_messages_modal): print $messages; endif; ?>

</div>
<!--/.page -->

<div id="accessibility" class="reveal-modal small" aria-hidden="true" aria-label="Accessibility Tools" data-reveal>
  <?php print render($page['a11y']); ?>
</div>
