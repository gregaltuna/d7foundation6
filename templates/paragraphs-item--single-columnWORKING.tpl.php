<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 *
 * Available variables:
 * - $content: An array of content items. Use render($content) to print them
 *   all, or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity
 *   - entity-paragraphs-item
 *   - paragraphs-item-{bundle}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened into
 *   a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>
<!-- PARAGRAPHS-ITEM.TPL.PHP -->
<!-- SINGLE COLUMN -->



<div class="section
  <?php if(isset($content['field_par_padding']['#items'][0]['value']) && !empty($content['field_par_padding']['#items'][0]['value'])): print ' '.($content['field_par_padding']['#items'][0]['value']); endif; ?>
  <?php if(isset($content['field_par_type_color']) && ($content['field_par_type_color']['#items'][0]['value'] == "1")): print ' dark-panel'; endif; ?>"
  <?php if(isset($content['field_par_bg_img']) && !empty($content['field_par_bg_img'])): print ' style="background: url(\'/sites/default/files/'.($content['field_par_bg_img']['#items'][0]['filename']).'\') center center no-repeat; background-size: cover;"';
  elseif(isset($content['field_par_bg_color']) && !empty($content['field_par_bg_color'])): print ' style="background-color: '.$content['field_par_bg_color']['#items'][0]['rgb'].'"'; endif; ?>>
  <div class="box<?php if(isset($content['field_par_wide']) && ($content['field_par_wide']['#items'][0]['value'] == "1")): print '-wide'; endif; ?>">
    <div class="row">
      <div class="columns small-12">
        <?php print render($content['field_par_content']); ?>
      </div>
    </div>
  </div>
</div>

<?php //dpm(get_defined_vars()); ?>
