<?php

/**
 * @file
 * Default theme implementation to display the basic html structure of a single
 * Drupal page.
 *
 * Variables:
 * - $css: An array of CSS files for the current page.
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.
 * - $rdf_namespaces: All the RDF namespace prefixes used in the HTML document.
 * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF data.
 * - $head_title: A modified version of the page title, for use in the TITLE
 *   tag.
 * - $head_title_array: (array) An associative array containing the string parts
 *   that were used to generate the $head_title variable, already prepared to be
 *   output as TITLE tag. The key/value pairs may contain one or more of the
 *   following, depending on conditions:
 *   - title: The title of the current page, if any.
 *   - name: The name of the site.
 *   - slogan: The slogan of the site, if any, and if there is no title.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $page_top: Initial markup from any modules that have altered the
 *   page. This variable should always be output first, before all other dynamic
 *   content.
 * - $page: The rendered page content.
 * - $page_bottom: Final closing markup from any modules that have altered the
 *   page. This variable should always be output last, after all other dynamic
 *   content.
 * - $classes String of classes that can be used to style contextually through
 *   CSS.
 *
 * @see template_preprocess()
 * @see template_preprocess_html()
 * @see template_process()
 */
?>
<!DOCTYPE html>
<!-- Sorry no IE7 support! -->
<!-- @see http://foundation.zurb.com/docs/index.html#basicHTMLMarkup -->

<!--[if IE 8]><html class="no-js lt-ie9" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>"> <!--<![endif]-->
<head>
  <?php print $head; ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <title><?php print $head_title; ?></title>

	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="//webassets.unt.edu/images/d7foundation/favicons/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

  <?php print $styles; ?>

  <!--link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"-->

	<!--link rel="shortcut icon" href="https://www.unt.edu/sites/all/themes/unt_foundation/favicons/favicon.ico"-->

	<?php print render($headcss); ?>

  <?php if (isset($field_manual_css)){
	  print '<style id="manual-css">';
    print $field_manual_css;
		print '</style>';
  } ?>

	<script>document.cookie='resolution='+Math.max(screen.width,screen.height)+'; path=/';</script>

  <?php print $scripts; ?>

  <!--script src="https://use.fontawesome.com/69a1bbba1e.js"></script-->
  <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js"></script>
  <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/v4-shims.js"></script>

	<?php print render($headjs); ?>

  <!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
  <style>

    /* header[role="banner"] {
      position: fixed;
    }

    .bap-wrapper {
      padding-top: 120px;
    } */
/*
    #hero-feature {
      padding-top: 120px !important;
    }

    .scrolled #hero-feature {
      padding-top: 45px !important;
    } */
  </style>
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
  <div id="skip" class="skip-link">
    <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
  </div>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
  <?php print _zurb_foundation_add_reveals(); ?>

	<?php print render($bodyjs); ?>

	<script type="text/javascript">

  // changes default template.php output of the google search form, to allow for different IDs on the input and aria-labels
  jQuery(document).ready(function() {
		jQuery("#dropbar #google-search-label").attr('for', 'wdc-google-search-scrolled');
		jQuery("#dropbar #google-search-label").attr('id', 'google-search-label-scrolled');
		jQuery("#dropbar #wdc-google-search").attr('id', 'wdc-google-search-scrolled');
		// FOR STANDARD DRUPAL SEARCH
		jQuery("#dropbar .form-item-search-block-form label").attr('for', 'edit-search-block-form--4-mobile');
		jQuery("#dropbar #edit-search-block-form--4").attr('id', 'edit-search-block-form--4-mobile');
  });

  // creates/adds scrolled classes
	jQuery(window).scroll(function() {

		// read scroll from top
		var jumpIf = 160; //75
		var scroll = jQuery(window).scrollTop();
		var desktop = window.matchMedia("(min-width: 770px)").matches;

		if(window.jumpOff > 0){
			if(scroll < window.jumpOff){
				window.jumpOff = 0;
				jQuery("body").removeClass('scrolled');
			}
		} else {
			if(jQuery('.header').length > 0){
				jumpIf = jQuery('.header').offset().top + 160; //75
			}
			if(scroll > jumpIf){
				jQuery("body").addClass('scrolled');
				window.jumpOff = jumpIf;
			}
		}
	});
	jQuery('.col').has('.col').addClass('parent');
	</script>

  <script>
    (function ($, Drupal, window, document, undefined) {
      $(document).foundation();
    })(jQuery, Drupal, this, this.document);
  </script>
</body>
</html>
