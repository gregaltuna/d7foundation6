<?php
/**
 * @file
 * Default theme implementation for beans.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) entity label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-{ENTITY_TYPE}
 *   - {ENTITY_TYPE}-{BUNDLE}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>

<?php //if(!empty($content['field_wide_grid'])): echo($content['field_wide_grid']['#items'][0]['value']); else: echo '8'; endif; ?>
<?php //if(!empty($content['field_medium_grid'])): echo($content['field_medium_grid']['#items'][0]['value']); else: echo '6'; endif; ?>
<?php //if(!empty($content['field_small_grid'])): echo($content['field_small_grid']['#items'][0]['value']); else: echo '4'; endif; ?>

<?php //if(isset($content['field_dark_overlay']) && !empty($content['field_dark_overlay'])): print 'style="background: rgba(0,0,0,.' . $content['field_dark_overlay']['#items'][0]['value'] . ');"'; endif; ?>
<?php //if(isset($content['field_background_align']) && !empty($content['field_background_align'])): print $content['field_background_align']['#items'][0]['value']; else: print 'center'; endif; ?>

<?php //foreach($content['field_gallery_images']['#items'] as $image){ echo $image['filename'];} ?>

<script type="text/javascript">
jQuery(window).on('resize', function(){
    jQuery('.gallery-image').each(function(){
        jQuery(this).height( this.getBoundingClientRect().width ) ;    
     });
});
</script>

<div class="section image-gallery <?php print str_replace(' ', '-', strtolower($title)); ?>">
<!--div class="section image-gallery"-->
	<div class="box<?php if(!empty($content['field_enable_wide_layout']) && ($content['field_enable_wide_layout']['#items'][0]['value']==1)): echo ' wide'; endif; ?>">
		<div class="row">
			
			<h2 class="title alt-block-title"><?php echo $title; ?></h2>
			<ul class="bean-gallery xlarge-block-grid-<?php if(!empty($content['field_xlarge_grid'])): echo($content['field_xlarge_grid']['#items'][0]['value']); else: echo '10'; endif; ?> large-block-grid-<?php if(!empty($content['field_large_grid'])): echo($content['field_large_grid']['#items'][0]['value']); else: echo '8'; endif; ?> medium-block-grid-<?php if(!empty($content['field_medium_grid'])): echo($content['field_medium_grid']['#items'][0]['value']); else: echo '6'; endif; ?> small-block-grid-<?php if(!empty($content['field_small_grid'])): echo($content['field_small_grid']['#items'][0]['value']); else: echo '4'; endif; ?>">

			<?php 
				$lastItem = count($content['field_gallery_images']['#items']);
				$counter = 1;
				
				foreach($content['field_gallery_images']['#items'] as $image){ 
			
					$prev = ($counter==1) ? $lastItem : $counter-1;
					$next = ($counter==$lastItem) ? 1 : $counter+1;
				
				echo 
				'<li>
				
					<div class="gallery-image-wrapper" style="background: url(/sites/default/files/' . $image['filename'] . ') center center no-repeat; background-size: cover; cursor: pointer;" data-reveal-id="thumb-' . $counter . '" style="cursor:pointer;"><div class="gallery-image">&nbsp;</div></div>
					
					<div id="thumb-' . $counter . '" class="reveal-modal large" data-reveal>
						<img src="/sites/default/files/' . $image['filename'] . '" alt="' . $image['alt'] . '">
						
						<div class="row">
			
							<div class="large-6 columns">
								<p class="gallery-prev"><a href="#" data-reveal-id="thumb-' . $prev . '" class="secondary button rounded"><i class="fa fa-chevron-left"></i> previous image</a></p>
							</div>
				
							<div class="large-6 columns" style="text-align:right;">
								<p class="gallery-next"><a href="#" data-reveal-id="thumb-' . $next . '" class="secondary button rounded">next image <i class="fa fa-chevron-right"></i></a></p>
							</div>
						</div>
						
						<p><a class="close-reveal-modal">×</a></p>
					</div>
					
				</li>';
				
					$counter++;
				}
			?>
			</ul>
			
		</div>
	</div>
</div>


<?php //dpm($content['field_gallery_images']); ?>
