<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 *
 * Available variables:
 * - $content: An array of content items. Use render($content) to print them
 *   all, or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity
 *   - entity-paragraphs-item
 *   - paragraphs-item-{bundle}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened into
 *   a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>
<!-- SINGLE COLUMN -->
<?php if(isset($content['field_par_overlay_layer']) && ($content['field_par_overlay_layer']['#items'][0]['value']) > 0 ): ?>
<style>
  div.section<?php if(isset($content['field_par_add_id']) && !empty($content['field_par_add_id']['#items'][0]['value'])): print '.'.($content['field_par_add_id']['#items'][0]['value']); endif; ?> {
    position: relative;
  }

  div.content-wrapper {
    z-index: 1;
    position: relative;
  }

  div.section<?php if(isset($content['field_par_add_id']) && !empty($content['field_par_add_id']['#items'][0]['value'])): print '.'.($content['field_par_add_id']['#items'][0]['value']); endif; ?>:after {
    content: "";
    background-color: rgba(0,0,0,.<?php print ($content['field_par_overlay_layer']['#items'][0]['value']); ?>);
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    position: absolute;
    z-index: 0;
  }
</style>
<?php endif; ?>

<div class="section
  <?php if(isset($content['field_par_add_id']) && !empty($content['field_par_add_id']['#items'][0]['value'])): print ' '.($content['field_par_add_id']['#items'][0]['value']); endif; ?>
  <?php if(isset($content['field_par_padding']['#items'][0]['value']) && !empty($content['field_par_padding']['#items'][0]['value'])): print ' '.($content['field_par_padding']['#items'][0]['value']); endif; ?>
  <?php if(isset($content['field_par_dark_panel']) && ($content['field_par_dark_panel']['#items'][0]['value'] == "1")): print ' dark-panel'; endif; ?>"
style="
  <?php
  if(isset($content['field_par_bg_img']) && !empty($content['field_par_bg_img'])):
    print ' background: url(\'/sites/default/files/'.($content['field_par_bg_img']['#items'][0]['filename']).'\') center center no-repeat; background-size: cover;';
  elseif(isset($content['field_par_bg_color']) && !empty($content['field_par_bg_color'])):
    print ' background-color: '.$content['field_par_bg_color']['#items'][0]['rgb'].';';
  endif; ?>
  <?php if(($content['field_par_bg_img_behavior']['#items'][0]['value'] == "parallax")): print ' background: transparent !important;'; endif; ?>
  <?php if(($content['field_par_bg_img_behavior']['#items'][0]['value'] == "fixed")): print ' background-attachment: fixed !important;'; endif; ?>
"
  <?php if(($content['field_par_bg_img_behavior']['#items'][0]['value'] == "parallax")): print ' data-parallax="scroll" data-image-src="/sites/default/files/'.($content['field_par_bg_img']['#items'][0]['filename']).'"'; endif; ?>
  >

  <?php if(($content['field_par_rem_container']['#items'][0]['value']) == 0): ?>
  <div class="box<?php if(isset($content['field_par_wide']) && ($content['field_par_wide']['#items'][0]['value'] == "1")): print '-wide'; endif; ?>">
    <div class="row">
      <div class="columns small-12">
  <?php endif; ?>
        <div class="content-wrapper">
          <?php if(isset($content['field_par_heading']['#items'][0]['value']) && !empty($content['field_par_heading']['#items'][0]['value'])):
            print '<'.($content['field_par_heading_size']['#items'][0]['value']).' class="title '.($content['field_par_heading_align']['#items'][0]['value']).'">' .($content['field_par_heading']['#items'][0]['value']).'</'.($content['field_par_heading_size']['#items'][0]['value']).'>';
          endif; ?>
          <?php print render($content['field_par_content']['#items'][0]['value']); ?>
        </div>
  <?php if(($content['field_par_rem_container']['#items'][0]['value']) == 0): ?>
      </div>
    </div>
  </div>
  <?php endif; ?>
</div>

<?php //dpm(get_defined_vars()); ?>
